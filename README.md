# fluent-plugin-udp-stream

Plug-in to output to udp stream.

## Download

[https://bitbucket.org/winebarrel/fluent-plugin-udp-stream/downloads/fluent-plugin-udp-stream-0.0.2.gem](https://bitbucket.org/winebarrel/fluent-plugin-udp-stream/downloads/fluent-plugin-udp-stream-0.0.2.gem)

## Installation

    $ gem install fluent-plugin-udp-stream-0.0.2.gem

## Configuration

```
<match any_tag.**>
  type copy
  <store>
    type udp_stream
    #host 127.0.0.1
    #port 25000
  </store>
  <store>
    type stdout
  </store>
</match>
```

## Usage

see [FluentUdpStream](https://bitbucket.org/winebarrel/fluent_udp_stream).

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
